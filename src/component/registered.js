import React from "react";
import { Message,Grid } from "semantic-ui-react";

const Registered = () => (

	<div style={{display:'flex',flexDirection:'column',alignItems:'center'}}>
	<div style={{width:'50vw'}}>
		<Message success>
					<h1>Terimakasih Anda Telah Terdaftar</h1>
				</Message>
	</div>
	
	</div>
);

export default Registered;
